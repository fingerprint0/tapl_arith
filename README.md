**Arith Grammar from TAPL** 

Parser and executor for the arith grammar from tapl book.

- Haskell implementation of parser and executor for the arith grammar in TAPL
- This is very much WIP and also a way for me to learn Haskell, but the eventual goal of this
  (apart form learning type systems is to write my own type checker for simply typed lambda
  calculus).  
