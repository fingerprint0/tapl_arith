{-# LANGUAGE OverloadedStrings #-}

module Terms where 

data Term
  = VZero 
  | VTrue
  | VFalse
  | Succ Term
  | Pred Term
  | IsZero Term
  | IfThenElse Term Term Term 
  deriving (Eq, Show)

