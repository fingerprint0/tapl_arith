{-# LANGUAGE OverloadedStrings #-}

module Printer
    ( arithPrinter
    ) where

import Terms 
import Data.Text (Text)

termPrinter :: Term -> Text -> Text
termPrinter VZero b = b <> "0"
termPrinter VTrue b = b <> "true"
termPrinter VFalse b = b <> "false"
termPrinter (Succ t) b = b <> "succ (" <> termPrinter t "" <> ")"
termPrinter (Pred t) b = b <> "pred (" <> termPrinter t "" <> ")"
termPrinter (IsZero t) b = b <> "iszero (" <> termPrinter t "" <> ")"
termPrinter (IfThenElse t0 t1 t2) b = b 
  <> "if " 
  <> termPrinter t0 "" 
  <> " then "
  <> termPrinter t1 ""
  <> " else "
  <> termPrinter t2 ""

arithPrinter :: Term -> Text -> Text
arithPrinter t b = termPrinter t b <> "\n"

