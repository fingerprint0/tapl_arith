{-# LANGUAGE OverloadedStrings #-}

module Evaluator
    ( arithEvaluator
      ,EvaluatorError
      ,getTerm
    ) where

import Terms 
import Data.Text (Text)
import Data.Maybe (maybe)
import Data.Either (fromLeft)
import Control.Monad (liftM3)
import qualified Data.List as L

newtype EvaluatorError = EvaluatorError {getTerm :: Term}

arithEvaluator' :: Term -> Either (Maybe Term) EvaluatorError
arithEvaluator' VZero = Left Nothing
arithEvaluator' VTrue = Left Nothing
arithEvaluator' VFalse = Left Nothing 

arithEvaluator' (Pred VZero) = Left $ Just VZero
arithEvaluator' (Pred (Succ t)) = Left $ Just t
arithEvaluator' (Pred VTrue) = Right $ EvaluatorError (Pred VTrue) 
arithEvaluator' (Pred VFalse) =  Right $ EvaluatorError (Pred VFalse)
arithEvaluator' (Pred (IsZero t)) = Right $ EvaluatorError (Pred (IsZero t))
arithEvaluator' (Pred t) = 
  case arithEvaluator' t of
    Left n -> Left (Pred <$> n)
    Right ee -> Right ee

arithEvaluator' (Succ VTrue) = Right $ EvaluatorError (Succ VTrue) 
arithEvaluator' (Succ VFalse) = Right $ EvaluatorError (Succ VFalse)
arithEvaluator' (Succ (IsZero t)) = Right $ EvaluatorError (IsZero t)
arithEvaluator' (Succ t) = 
  case arithEvaluator' t of 
    Left t' -> Left (Succ <$> t')
    Right ee -> Right ee

arithEvaluator' (IsZero VZero) = Left $ Just VTrue
arithEvaluator' (IsZero (Succ t)) = Left $ Just VFalse
arithEvaluator' (IsZero t) = 
  case arithEvaluator' t of
    Left t' -> Left (IsZero <$> t')
    Right ee -> Right ee

arithEvaluator' (IfThenElse VTrue t1 t2) = Left $ Just t1
arithEvaluator' (IfThenElse VFalse t1 t2) = Left $ Just t2
arithEvaluator' (IfThenElse VZero t1 t2) = Right $ EvaluatorError (IfThenElse VZero t1 t2)
arithEvaluator' (IfThenElse t0 t1 t2) = 
  case arithEvaluator' t0 of
    Left t' -> Left (liftM3 IfThenElse t' (return t1) (return t2))
    Right ee -> Right ee

arithEvaluator :: Term -> Either Term EvaluatorError 
arithEvaluator t = 
  case arithEvaluator' t of
    Left (Just t') -> arithEvaluator t'
    Left Nothing -> Left t
    Right ee -> Right ee


