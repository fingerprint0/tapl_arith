{-# LANGUAGE OverloadedStrings #-}

module Parser 
    ( parseArith
    ) where

import Terms 
import Text.Megaparsec
import Data.Void (Void)
import Text.Megaparsec.Char (space1)
import Data.Text (Text, pack, unpack)
import Control.Monad.Combinators.Expr
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void Text 

-- helper function to parse white spaces
ws :: Parser ()
ws = L.space space1 (L.skipLineComment "//") (L.skipBlockComment "/*" "*/")

-- helper function to parse lexemes i.e. decimal / int etc.
lexeme :: Parser a -> Parser a 
lexeme = L.lexeme ws

-- helper function to parse symbols i.e. specific strings
symbol :: Text -> Parser Text
symbol = L.symbol ws

-- helper function to parse brackets 
-- between is a Megaparsec function
brackets :: Parser a -> Parser a
brackets = between (symbol "(") (symbol ")")

integer :: Parser Integer
integer = lexeme L.decimal 

pVal :: Parser Term
pVal = choice $ try <$> [ VZero <$ symbol "0"
                          , VTrue <$ symbol "true"
                          , VFalse <$ symbol "false"
                         ]

pSucc :: Parser Term 
pSucc = do
  _ <- symbol "succ"
  Succ <$> termParser
  
pPred :: Parser Term 
pPred = do 
  _ <- symbol "pred"
  Pred <$> termParser

pIsZero :: Parser Term 
pIsZero = do
  _ <- symbol "iszero"
  IsZero <$> termParser

pIfThenElse :: Parser Term
pIfThenElse = do 
  _ <- symbol "if"
  pred <- termParser
  _ <- symbol "then"
  thenCase <- termParser
  _ <- symbol "else"
  elseCase <- termParser
  return (IfThenElse pred thenCase elseCase)

termParser :: Parser Term
termParser = choice $ try <$> [ brackets termParser 
                                , pVal
                                , pSucc 
                                , pPred
                                , pIsZero
                                , pIfThenElse
                               ]

parseArith :: String -> Text -> Either String Term 
parseArith file term = 
  case runParser termParser file term of
    Left a -> Left (show a)
    Right a -> Right a


