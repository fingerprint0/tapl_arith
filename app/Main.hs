{-# LANGUAGE OverloadedStrings #-}

module Main where

import Terms
import Parser 
import Printer
import Evaluator
import Data.Text (Text)
import System.Environment as SE (getArgs)
import qualified Data.List as L (foldr, map)
import qualified Data.Text as Text (lines, pack, unpack, splitOn)
import qualified Data.Text.IO as Text (readFile, writeFile, putStrLn, hGetLine)

main :: IO ()
main = do 
  args <- getArgs
  let fileName = head args
  file <- Text.readFile fileName
  Text.putStrLn "Received terms ... "
  Text.putStrLn $ printTerms $ parseTerms fileName (Text.lines file)
  Text.putStrLn " ... which evaluate to "
  Text.putStrLn $ printEvals $ evaluateTerms $ parseTerms fileName (Text.lines file)

parseTerms :: String -> [Text] -> [Term]
parseTerms file = map f  
  where f t = case parseArith file t of 
                Left e -> error e
                Right p' -> p'

printTerms :: [Term] -> Text
printTerms = L.foldr f ""
  where f t = arithPrinter t 

evaluateTerms :: [Term] -> [Either Term EvaluatorError]
evaluateTerms = L.map f
  where f t = arithEvaluator t 

printEvals :: [Either Term EvaluatorError] -> Text
printEvals = L.foldr f ""
  where f et b = case et of
                 Left t -> b <> "PASS: " <> printTerms [t]
                 Right ee -> b <> "FAILED @: " <> printTerms [getTerm ee]

